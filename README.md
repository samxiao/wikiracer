Wikiracer
---

This problem is designed to test your abilities to do the kind of systems programming we do, with a focus on concurrency, networking, performance and some algorithmic optimizations.

#### Problem

There’s an online form of racing called Wikiracing.  The goal is to traverse your way from one Wikipedia page to another, using only links.

Here’s an example path from `Mike Tyson` to `Segment` which can be found in about a second in four hops:

    Mike Tyson → Alexander the Great
    Alexander the Great → Greek language
    Greek language → Fruit anatomy
    Fruit anatomy → Segment

We want to try to complete a Wikirace as quickly as possible without requiring a complete graph of Wikipedia because Wikipedia pages are changing all the time. Feel free to take this as an opportunity to showcase your knowledge of network programming and concurrent/parallel programming. You can scrape HTML pages, but we’d suggest taking a peek at what the MediaWiki API has to offer (or do both, and benchmark them against each other).

Your Wikiracer should take a starting page and an end page (as either a URL or a page title) and successfully figure out how to traverse from one to the other (or tell the user if there isn’t a path). The output should be a list of paths, as well as the total elapsed time to run. Watch out for pathological edge cases and handle them gracefully.

Your Wikiracer gets points for having a low wall-clock time, not for the lowest number of hops. You’ll want to optimize the wall-clock time speed of reaching one page to the other.

There are many ways to build a Wikiracer, but we’d especially like it if yours had a REST API.

We love all kinds of programming languages. We’re currently building all our services in Go and Node, and we’re excited to work with you if you know these languages or think you could pick them up reasonably quickly. For this exercise, it would be great if you could use either Go or Node to implement it, since that gives us the best opportunity to have a deeply technical conversation with you. If you’re currently more comfortable with Java, C/C++, Python, Ruby or other languages in those families, we’re happy to make it work — We has many fans of all of the above on the team.

Feel free to use any libraries that you think might aid your cause. Be ready to walk us through why you chose your specific implementation language and libraries — we’d love to understand the decisions you made here.

### To get it

    $ go get gitlab.com/samxiao/wikiracer


### Install Dependencies

    # Basic
    $ brew install go
    $ brew install docker
    $ brew install docker-compose

    # Glide
    $ brew install glide
    $ glide install

### Running Dependencies

    $ docker-compose up

### BUILD

    $ make

### RUN

    $ ./wikirace -starttitle "GitLab" -endtitle "Pebble Time" -config "config.yaml"

Put a `time` will also check the time it allows to run:

    $ time ./wikirace -starttitle "GitLab" -endtitle "Pebble Time" -config "config.yaml"


## Design

This starts as a CLI that takes 2 arguments : a Wiki title that starts the search and a Wiki title that ends the search.  It also takes `-loglevel` flag for logging level for the output.

The setup is configuration-driven, meaning that the CLI also takes a `-config` for configuration to setup the required Wiki API endpoint, and Redis DB, etc.


Since Wiki has provided API for [searching forward](https://www.mediawiki.org/wiki/API:Links) and [searching backward](https://www.mediawiki.org/wiki/API:Linkshere).  Finding a mid point will be at least traverse much efficiently.

I had never interested in parsing each Wiki page as HTML and scrap from it. I found that Wiki has API to return as JSON format which is much easier to parse in Go.  I do know the "nice-to-have" is to provide any web page and crawl, but it is not the primary focus of this exercise.  However, I structure Wikiracer in a way that can be easily extended. Check out the plugin model and the `lib/wiki/config_test.yaml` on plugin type as `wiki` to see how it works now.

At the beginning of the search, it starts 2 Goroutine functions that asynchronously perform search from forward direction and backward direction.  Inside each directional search, it first gets the current list of related titles from a given title, then create a worker pool with Goroutine to perform processing to find a match.  It finds a match by checking whether the reverse direction has already visited.  This can be easily done via keeping track of 2 Hashmaps of the visited title.

However, Golang's Map is not Goroutine-safe, so I build another one with `Sync.Mutex` to allow that. At later I found having it done via Redis is better designed for the future when we move the worker outside of Goroutine into individual job workers. That's the model inspired from similar setup from Python's Celery or Ruby's Sidekiq ecosystem. It can also scale better when workers are distributed. I also consider adding Zookeeper/RabbitMQ for distributing and coordinating workers across a cluster that can be benefit from this Fan-Out problem.  But again, the main focus is to solve the problem first.

After a search is found, it then adds to the a stack (also backed by Redis) which is used for constructing the call path as a graph.

When a research result is not find, it will continue performing the search by recursively calling itself.


## Progress

- 3 hrs : Play around with Wiki's API on query and immediately knows the single-directional search will not be the most optimal algorithm. Simply using `curl` incombination with `jq` to get familiar with the HTTP RESTful APIs.
- 1.5 hrs : Wrote the HTTP client for Wiki and build few functions to make the Wiki search query by getting a list of related Wiki title pages as result.
- 2 hrs : Building the algorithm for the search by first realizing it is a Fan-Out/Fan-In problem with multiple processes running parallel.  The solution works but very slow.
- 1.5 hrs : Adds some test cases on query and search to help to find another way to optimize the search performance.
- 2 hrs : Research and implement a simple Worker Pool via Goroutine to simulate that it would be like in a prod-ready environment.
- 2 hrs : Move caches and stacks data structure to Redis which gets it ready for distributed job-workers architecture.
- 2 hrs : Rewrite to simply the HTTP query and search functions which controlled by a direction struct instead of having 2 functions that does the search, and 2 functions that does the query.  This also makes the code more readable and easy to reason about.
- 1.5 hrs : Create a Makefile to fix all the linter issues, and create an auto-versioning for each build.
- 0.5 hr : Add support of Context for easier Goroutine cancellation on HTTP/Redis client executions.
- 1 hr : Adding proper logging
- 0.5 hr : Code clean up




## Few things I did not do, but will do :

- Continue research and refine a better search algorithm for optimization.
- Prefer use something like [Machinery](https://github.com/RichardKnop/machinery) framework become a real worker pool implementation backed by distributed system.
	- Ref: [Using Machinery as a sample](https://github.com/masnun/golang-distributed-task-processing)
- Only takes a Wiki title, but not the full wiki URL (easy to change) or any web page.
- Has logging, but no metrics/tracing yet (I fully understand why we need them in prod, and I always use them in prod)
- No Zookeeper/Etcd (fully understand the reason when it's required for microservices)
- CLI only, but can easily convert it to a web service that takes RESTful API endpoint (easy to change on Go)
