package lib

// SearchQuery : Abstraction object which holds the searching wiki target
// from start to end for all plugins (e.g. wiki, html, etc)
type SearchQuery struct {
	StartTitle string
	EndTitle   string
}

// SearchResultPair : Storing the result as a graph pair
// (source -> destination)
type SearchResultPair struct {
	Src  string
	Dest string
}

// SearchResultGraph : A list of SearchResultPairs
type SearchResultGraph []SearchResultPair

// NewSearchResultGraph : Builds a new instance of SearchResultGraph
func NewSearchResultGraph() SearchResultGraph {
	s := make([]SearchResultPair, 0)
	return s
}
