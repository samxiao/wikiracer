package wiki

import (
	"io/ioutil"
	"runtime"
	"sync"

	log "github.com/sirupsen/logrus"

	"golang.org/x/net/context"
	yaml "gopkg.in/yaml.v2"

	"gitlab.com/samxiao/wikiracer/clients"
	"gitlab.com/samxiao/wikiracer/lib"
)

// FunctionInterface : An abstraction that holds all the functions.  It is used
// for writing easier unit testing for mocking.
type FunctionInterface interface {
	Search(
		context.Context,
		context.CancelFunc,
		QueryInterface,
		*SearchDirection,
		string) string
	BuildSearchResultGraph(
		string, string) (lib.SearchResultGraph, error)
	Worker(
		context.Context,
		context.CancelFunc,
		QueryInterface,
		*SearchDirection,
		int,
		<-chan string,
		chan<- string,
		chan<- error)
}

// Config : configuration for using wikiracer.
type Config struct {
	RedisConfig struct {
		Address  string `yaml:"address"`
		Password string `yaml:"password"`
		Cache    struct {
			BackwardDB int `yaml:"backward_db"`
			ForwardDB  int `yaml:"forward_db"`
		} `yaml:"cache"`
		Stack struct {
			DB int `yaml:"db"`
		} `yaml:"stack"`
	} `yaml:"redis"`
}

type (
	// Wiki : An object that holds all the necessary data structures for
	// wikiracer.
	Wiki struct {
		Logger *log.Logger

		// For Redis Caching on searched title page
		ForwardCacheRedisClient  *clients.Redis
		BackwardCacheRedisClient *clients.Redis

		// Keep track for found path to construct a graph
		StackRedisClient *clients.Redis
	}

	// SearchDirection : Struct that holds all the metadata for a direction
	// the Wiki query API is searching.
	SearchDirection struct {
		Name               string
		CurrentRedisClient *clients.Redis
		ReverseRedisClient *clients.Redis
	}

	// - - - Forward search query result data structure - - -

	// Links : Maps to Wiki API - "links" for unmarshalling from the JSON body.
	Links struct {
		Continue      Plcontinue `json:"continue"`
		Batchcomplete string     `json:"batcomplete"`
		Query         PlQuery    `json:"query"`
	}

	// Plcontinue : Maps to Wiki API - "plcontinue" for unmarshalling from
	// the JSON body.
	Plcontinue struct {
		Continue   string `json:"continue"`
		Plcontinue string `json:"plcontinue"`
	}

	// PlQuery : Maps to Wiki API for unmarshalling from the JSON body.
	PlQuery struct {
		PageIds []string               `json:"pageids"`
		Pages   map[string]PlPageLinks `json:"pages"`
	}

	// PlPageLinks : Maps to Wiki API for unmarshalling from the JSON body.
	PlPageLinks struct {
		Links []Link `json:"links"`
		Title string `json:"title"`
	}

	// - - - Backward search query result data structure - - -

	// Linkshere : Maps to Wiki API - "linkshere" for unmarshalling from the
	// JSON body.
	Linkshere struct {
		Continue      Lhcontinue `json:"continue"`
		Batchcomplete string     `json:"batchcomplete"`
		Query         LhQuery    `json:"query"`
	}

	// Lhcontinue : Maps to Wiki API - "lhcontinue" for unmarshalling from the
	// JSON body.
	Lhcontinue struct {
		Continue   string `json:"continue"`
		Lhcontinue string `json:"lhcontinue"`
	}

	// LhQuery : Maps to Wiki API for unmarshalling from the JSON body.
	LhQuery struct {
		PageIds []string               `json:"pageids"`
		Pages   map[string]LhPageLinks `json:"pages"`
	}

	// LhPageLinks : Maps to Wiki API for unmarshalling from the JSON body.
	LhPageLinks struct {
		Linkshere []Link `json:"linkshere"`
		Title     string `json:"title"`
	}

	// - - - Other common search query result data structure - - -

	// Link : Maps to Wiki API for unmarshalling from the JSON body.
	Link struct {
		Title string `json:"title"`
	}
)

// NewWiki : A new Wiki object instance that we can use to run wikiracer.
func NewWiki(logger *log.Logger, configFilePath string) (*Wiki, error) {
	// Loading Wiki configs
	var wikiConfig Config
	data, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, err
	}
	if err := yaml.Unmarshal(data, &wikiConfig); err != nil {
		return nil, err
	}

	// Setup Redis clients
	redisClientForwardCache, err := clients.NewRedisClient(
		wikiConfig.RedisConfig.Address,
		wikiConfig.RedisConfig.Password,
		wikiConfig.RedisConfig.Cache.ForwardDB)
	if err != nil {
		return nil, err
	}

	redisClientBackwardCache, err := clients.NewRedisClient(
		wikiConfig.RedisConfig.Address,
		wikiConfig.RedisConfig.Password,
		wikiConfig.RedisConfig.Cache.BackwardDB)
	if err != nil {
		return nil, err
	}

	redisClientStack, err := clients.NewRedisClient(
		wikiConfig.RedisConfig.Address,
		wikiConfig.RedisConfig.Password,
		wikiConfig.RedisConfig.Stack.DB)
	if err != nil {
		return nil, err
	}

	// Setup Wiki instance
	wiki := &Wiki{
		StackRedisClient: redisClientStack,

		ForwardCacheRedisClient:  redisClientForwardCache,
		BackwardCacheRedisClient: redisClientBackwardCache,
	}
	wiki.Logger = logger
	return wiki, nil
}

// Search : Searching with a given direction to find a common mid-point that
// aligns with the reverse search. So it is more optimized than a
// single-directional search.  This only works when both backward and forward
// searches are running in parallel that finds path.
func (w *Wiki) Search(
	ctx context.Context,
	cancel context.CancelFunc,
	qi QueryInterface,
	direction *SearchDirection,
	title string) string {

	defer cancel()
	w.Logger.Debugln("In", direction.Name, "func() STARTED on ", title)

	var err error

	// Returns all links from the given pages.
	var titles []string

	var fn FetchFunc
	if err = GetQueryFetchFunc(qi, direction.Name, &fn); err != nil {
		w.Logger.Fatalln(err)
		return ""
	}
	err = fn(
		ctx,
		cancel,
		map[string]string{},
		title,
		&titles)

	if direction.ReverseRedisClient.Contains(ctx, title) {
		return title
	} else if err != nil ||
		len(titles) == 0 ||
		direction.CurrentRedisClient.Contains(ctx, title) {
		return ""
	} else {
		// Put the newly searched title as visited in cache
		direction.CurrentRedisClient.Set(ctx, title, "visited")
	}

	// logging
	w.Logger.WithFields(log.Fields{
		title: titles,
	})
	w.Logger.Debugln(title, titles)

	// - - - Goroutine worker pool setup - - -
	// followed guide : https://brandur.org/go-worker-pool
	jobs := make(chan string, 1000)
	results := make(chan string, 1000)
	errors := make(chan error, 1)

	var wg sync.WaitGroup

	// This starts up with X number of workers by number of the CPU,
	// initially blocked becasue there are no jobs.

	// Each titles has varies length, so let's take a minimum of number of
	// goroutine we can spawn before we either hit of having too many workers
	// spawned or too little which caused WaitGroup to have negative numbers.
	for worker := 1; worker <= min(len(titles), runtime.NumCPU()); worker++ {
		wg.Add(1) // inspired by : https://stackoverflow.com/a/35602585/618563
		go w.Worker(ctx, cancel, &wg, qi, direction, worker, jobs, results, errors)
	}

	// Here we send X `jobs` and then `close` that
	// channel to indicate that's all the work we have.
	for _, t := range titles {
		jobs <- t
	}
	close(jobs)
	wg.Wait()

	// Finally we collect all the results of the work.
	select {
	case err := <-errors:
		w.Logger.Errorln("finished with error:", err.Error())
	case r := <-results:
		if r != "" {
			w.Logger.Debugln("found result : ", r)
			return r
		}
		w.Logger.Debugln("no results found : ", r)
		break
	default:
		break
	}

	w.Logger.Debugln("In", direction.Name, "func() before calling recursively ", title)
	for _, t := range titles {
		// did not find it, continue the search recursively
		result := w.Search(ctx, cancel, qi, direction, t)
		if result != "" {
			w.StackRedisClient.Push(ctx, direction.Name, t)
			return result
		}
	}
	return ""
}

// Worker : Worker in a Go worker pool that runs as a Goroutine to process a
// search in Wiki title page. Based on the result to find a matching mid
// point.  It can be used by passing a Direction object to search
// bidirectionally.
func (w *Wiki) Worker(
	ctx context.Context,
	cancel context.CancelFunc,
	wg *sync.WaitGroup,
	qi QueryInterface,
	direction *SearchDirection,
	id int,
	jobs <-chan string,
	results chan<- string,
	errors chan<- error) {

	defer wg.Done()
	title := <-jobs
	w.Logger.Debugln(direction.Name, "worker", id, "started processing title page : ", title)

	var err error

	// returns all links from the given pages.
	var titles []string
	var fn FetchFunc
	if err = GetQueryFetchFunc(qi, direction.Name, &fn); err != nil {
		w.Logger.Fatalln(err)
		results <- ""
	}

	err = fn(
		ctx,
		cancel,
		map[string]string{},
		title,
		&titles)

	if err != nil ||
		len(titles) == 0 {
		results <- ""
	}

	// Check the first degree of related pages as it already obtained this list
	for _, t := range titles {
		if direction.CurrentRedisClient.Contains(ctx, t) {
			results <- ""
		} else {
			direction.CurrentRedisClient.Set(ctx, t, "visited")
		}
		if direction.ReverseRedisClient.Contains(ctx, t) {
			results <- t
		}
	}
	// nothing finds
	results <- ""
	w.Logger.Debugln(direction.Name, "worker", id, "completed processing title page : ", title)
}

// BuildSearchResultGraph : Takes 2 Stacks and 2 Title names, construct a
// a graph that represents how a wiki Title walks its best possible path to
// locate another wiki Title.
func (w *Wiki) BuildSearchResultGraph(
	ctx context.Context,
	startTitle string,
	endTitle string) (lib.SearchResultGraph, error) {

	forwardSize := w.StackRedisClient.Len(ctx, "forward")
	backwardSize := w.StackRedisClient.Len(ctx, "backward")
	w.Logger.Debugln("forward stack size: ", forwardSize)
	w.Logger.Debugln("backward stack size: ", backwardSize)

	// Cannot found
	if forwardSize == 0 && backwardSize == 0 {
		return nil, nil
	}

	var path []string
	path = append(path, startTitle)
	for i := 0; i < w.StackRedisClient.Len(ctx, "forward"); i++ {
		result, err := w.StackRedisClient.Pop(ctx, "forward")
		if result != "" && err == nil {
			path = append(path, result)
		}
	}
	for i := 0; i < w.StackRedisClient.Len(ctx, "backward"); i++ {
		result, err := w.StackRedisClient.Pop(ctx, "backward")
		if result != "" && err == nil {
			path = append(path, result)
		}
	}
	path = append(path, endTitle)

	graph := lib.NewSearchResultGraph()
	for k := 0; k < len(path)-1; k++ {
		item := &lib.SearchResultPair{
			Src:  path[k],
			Dest: path[k+1],
		}
		graph = append(graph, *item)
	}
	w.Logger.Debugln(graph)
	return graph, nil
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
