package wiki

import (
	"sync"
	"testing"

	"golang.org/x/net/context"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	log "github.com/sirupsen/logrus"
)

const (
	wikiConfigFilePath = "./config_test.yaml"
)

var mockForwardTitles = map[string][]string{
	"GitLab":           {"7 Cups", "9GAG", "Pebble (watch)", "SpaceX", "Songkick", "Sony"},
	"Pebble (watch)":   {"Kickstarter", "Runkeeper", "Pebble Time", "Magnetometer", "IFixit"},
	"Pebble Time":      {"E-paper", "Pebble (watch)", "RunKeeper", "Uber (company)"},
	"E-paper":          {"Electronic paper"},
	"Electronic paper": {"Pebble (watch)"},
	// other non-critical path maps
	"7 Cups":               {"Zapier", "Weebly", "Watsi"},
	"9GAG":                 {"Reddit", "Scribd"},
	"Kickstarter":          {"Veronica Mars (film)", "Runkeeper"},
	"Runkeeper":            {"ASICS"},
	"Uber (company)":       {"Ride sharing", "Qualcomm"},
	"Zapier":               {"Vote.org", "WePay"},
	"Weebly":               {"Webflow", "7 Cups", "9GAG"},
	"Watsi":                {"Algolia"},
	"Reddit":               {"4chan"},
	"Scribd":               {"Adobe Acrobat"},
	"Veronica Mars (film)": {"Blackmailing", "Audiobook"},
	"ASICS":                {"Haglöfs", "Boston"},
	"Ride sharing":         {"Carpool"},
	"Qualcomm":             {"3G", "AT&T"},
}

var mockBackwardTitles = map[string][]string{
	"E-reader":    {"Kindle Store", "E Ink", "Smartwatch"},
	"Smartwatch":  {"Mobile comic", "Samsung"},
	"Pebble Time": {"Pebble (watch)", "Apple Watch"},
	// other non-critical path maps
	"Kindle Store":      {"Audible Inc.", "CDNow"},
	"E Ink":             {"OLED", "Pepper's ghost", "Pebble (watch)"},
	"Mobile comic":      {"Manga", "Telephone tapping"},
	"Samsung":           {"Apple Inc.", "ICANN"},
	"Audible Inc.":      {"43 Things", "LoveFilm"},
	"CDNow":             {"LibraryThing", "Mobipocket", "Amazon S3"},
	"OLED":              {"Holography"},
	"Pepper's ghost":    {"Loudspeaker", "Holography"},
	"Manga":             {"Katakana", "Greyhound"},
	"Telephone tapping": {"Wiretapping", "Rootkit"},
	"Apple inc.":        {"Bluetooth Special Interest Group", "Computer mouse"},
	"ICANN":             {".moe", "Domain name registry"},
	"43 Things":         {"Tony Hsieh", "Amazon Video", "Shelfari"},
}

type MockQuery struct {
	sync.RWMutex
	mock.Mock
}

func (m *MockQuery) ForwardFetchTitles(
	ctx context.Context,
	cancel context.CancelFunc,
	extraQueryParams map[string]string,
	title string,
	titles *[]string) error {

	*titles = mockForwardTitles[title]
	args := m.Mock.Called()
	return args.Error(0)
}

func (m *MockQuery) BackwardFetchTitles(
	ctx context.Context,
	cancel context.CancelFunc,
	extraQueryParams map[string]string,
	title string,
	titles *[]string) error {

	*titles = mockBackwardTitles[title]
	args := m.Mock.Called()
	return args.Error(0)
}

func TestNewWiki(t *testing.T) {
	logger := log.New()
	w, err := NewWiki(logger, wikiConfigFilePath)
	assert.NotNil(t, w)
	assert.Nil(t, err)
}

func TestForwardSearch(t *testing.T) {
	logger := log.New()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	m := new(MockQuery)
	m.On("ForwardFetchTitles").Return(nil)
	m.On("BackwardFetchTitles").Return(nil)

	w, err := NewWiki(logger, wikiConfigFilePath)
	assert.Nil(t, err)
	assert.NotNil(t, w)

	direction := &SearchDirection{
		Name:               "forward",
		CurrentRedisClient: w.ForwardCacheRedisClient,
		ReverseRedisClient: w.BackwardCacheRedisClient,
	}

	// Pre-fill backward search result
	for k, v := range mockBackwardTitles {
		for i := range v {
			w.BackwardCacheRedisClient.Set(ctx, v[i], "visited")
		}
		w.BackwardCacheRedisClient.Set(ctx, k, "visited")
	}

	defer w.ForwardCacheRedisClient.FlushDB(ctx)
	defer w.BackwardCacheRedisClient.FlushDB(ctx)
	defer w.StackRedisClient.FlushDB(ctx)

	result := w.Search(ctx, cancel, m, direction, "GitLab")
	assert.Equal(t, "Pebble (watch)", result)
}

func TestBackwardSearch(t *testing.T) {
	logger := log.New()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	m := new(MockQuery)
	m.On("ForwardFetchTitles").Return(nil)
	m.On("BackwardFetchTitles").Return(nil)

	w, err := NewWiki(logger, wikiConfigFilePath)
	assert.Nil(t, err)
	assert.NotNil(t, w)

	direction := &SearchDirection{
		Name:               "backward",
		CurrentRedisClient: w.BackwardCacheRedisClient,
		ReverseRedisClient: w.ForwardCacheRedisClient,
	}
	// Pre-fill forward search result
	for k, v := range mockForwardTitles {
		for i := range v {
			w.ForwardCacheRedisClient.Set(ctx, v[i], "visited")
		}
		w.ForwardCacheRedisClient.Set(ctx, k, "visited")
	}

	defer w.ForwardCacheRedisClient.FlushDB(ctx)
	defer w.BackwardCacheRedisClient.FlushDB(ctx)
	defer w.StackRedisClient.FlushDB(ctx)

	result := w.Search(ctx, cancel, m, direction, "E-reader")
	assert.Equal(t, "Pebble (watch)", result)
}
