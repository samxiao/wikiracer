package wiki

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	log "github.com/sirupsen/logrus"

	yaml "gopkg.in/yaml.v2"

	"gitlab.com/samxiao/wikiracer/clients"
	"gitlab.com/samxiao/wikiracer/lib/utils"
	"golang.org/x/net/context"
)

// QueryInterface : Defining some query specific functions
type QueryInterface interface {
	ForwardFetchTitles(
		context.Context,
		context.CancelFunc,
		map[string]string,
		string,
		*[]string) error
	BackwardFetchTitles(
		context.Context,
		context.CancelFunc,
		map[string]string,
		string,
		*[]string) error
}

// FetchFunc : A generic function that share the same signature as other 2
// fetch functions.  It is used mainly to find a Wiki search querying function
// from registry reference by a name.
type FetchFunc func(
	context.Context,
	context.CancelFunc,
	map[string]string,
	string,
	*[]string) error

// GetQueryFetchFunc : Select a Wiki query search by provided name.  It goes
// to an internal registry and find it inside a map.
func GetQueryFetchFunc(q QueryInterface, key string, fn *FetchFunc) error {

	// The function registry
	var QueryFuncRegistry = map[string]FetchFunc{
		"forward":  q.ForwardFetchTitles,
		"backward": q.BackwardFetchTitles,
	}

	// get the function out of our function registry
	found := QueryFuncRegistry[key]
	if found == nil {
		// return a descriptive error if we can't find the function
		errMsg := fmt.Sprintf("unknown function in (*FetchFunc)Query: %s", key)
		return errors.New(errMsg)
	}

	// dereference the pointer receiver, so that the changes are visible to the caller
	*fn = found
	return nil
}

// Keep these explicitly here as they are API related to MediaWiki which hardly
// changes. If they do changes, code often needs to be updated anyway.  It is
// safer to keep them here along with the code, then let user changes them on
// configuration files.
var queryParams = map[string]string{
	"format":       "json",
	"action":       "query",
	"indexpageids": "true",
}

var wikiForwardQueryParams = map[string]string{
	"prop":        "links",
	"plnamespace": "0", // starts with the simplest case
	"pllimit":     "max",
	"pldir":       "ascending", // hopefully it is sorted
}

var wikiBackwardQueryParams = map[string]string{
	"prop":        "linkshere",
	"lhnamespace": "0", // starts with the simplest case
	"lhlimit":     "max",
}

const (
	wikiForwardContinueKey  = "plcontinue"
	wikiBackwardContinueKey = "lhcontinue"
	wikiTitlesKey           = "titles"
)

type (
	// QueryConfig : Configuration object that stores the HTTP request params.
	QueryConfig struct {
		HTTPConfig struct {
			APIEndPoint string `yaml:"api_end_point"`
			Timeout     int    `yaml:"timeout"`
			UserAgent   string `yaml:"user_agent"`
		} `yaml:"http"`
	}

	// Query : An abstraction that holds an instance of the HTTP client
	Query struct {
		Logger *log.Logger
		// for making HTTP client calls
		httpClient *clients.HTTPClient
	}
)

// NewQuery : Create a new instance of Query object by providing a
// configuration file.
func NewQuery(logger *log.Logger, configFilePath string) (*Query, error) {
	var config QueryConfig
	data, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, err
	}
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	q := &Query{
		httpClient: &clients.HTTPClient{
			APIEndPoint: config.HTTPConfig.APIEndPoint,
			UserAgent:   config.HTTPConfig.UserAgent,
			Timeout:     config.HTTPConfig.Timeout,
		},
	}
	q.Logger = logger
	return q, nil
}

// ForwardFetchTitles : Making a HTTP query request to Wiki and fetch all the
// links from a given page title. (Links in Wiki's API)
func (q *Query) ForwardFetchTitles(
	ctx context.Context,
	cancel context.CancelFunc,
	extraQueryParams map[string]string,
	title string,
	titles *[]string) error {

	defer cancel()
	qp := utils.NewConcurrentMap()
	qp.Merge(queryParams)
	qp.Merge(wikiForwardQueryParams)
	qp.Set(wikiTitlesKey, title)
	if extraQueryParams != nil {
		qp.Merge(extraQueryParams)
	}

	links := Links{}
	err := queryForLinks(ctx, cancel, q.httpClient, qp.List(), &links)
	if err != nil {
		return err
	}

	PrimaryPagesID := links.Query.PageIds[0] // it will only ever has 1 entry
	allLinks := links.Query.Pages[PrimaryPagesID].Links

	appendLinksToSlice(allLinks, titles)

	// API result may signal to continue because there are more properties to
	// retrieve for the pages so far. Let's handle it as proper pagination
	// using recursion.
	if links.Continue != (Plcontinue{}) {
		continueParams := map[string]string{
			wikiForwardContinueKey: links.Continue.Plcontinue,
		}
		err := q.ForwardFetchTitles(ctx, cancel, continueParams, title, titles)
		if err != nil {
			return err
		}
	}
	return nil
}

// BackwardFetchTitles : Making a HTTP query request to Wiki and fetch all the
// pages to a given page tile.  (Linkshere in Wiki's API)
func (q *Query) BackwardFetchTitles(
	ctx context.Context,
	cancel context.CancelFunc,
	extraQueryParams map[string]string,
	title string,
	titles *[]string) error {

	defer cancel()
	qp := utils.NewConcurrentMap()
	qp.Merge(queryParams)
	qp.Merge(wikiBackwardQueryParams)
	qp.Set(wikiTitlesKey, title)
	if extraQueryParams != nil {
		qp.Merge(extraQueryParams)
	}
	links := Linkshere{}
	err := queryForLinks(ctx, cancel, q.httpClient, qp.List(), &links)
	if err != nil {
		return err
	}

	PrimaryPagesID := links.Query.PageIds[0] // it will only ever has 1 entry
	allLinks := links.Query.Pages[PrimaryPagesID].Linkshere

	appendLinksToSlice(allLinks, titles)

	// API result may signal to continue because there are more properties to
	// retrieve for the pages so far. Let's handle it as proper pagination
	// using recursion.
	if links.Continue != (Lhcontinue{}) {
		continueParams := map[string]string{
			wikiBackwardContinueKey: links.Continue.Lhcontinue,
		}
		err := q.BackwardFetchTitles(ctx, cancel, continueParams, title, titles)
		if err != nil {
			return err
		}
	}
	return nil
}

// queryForLinks : Provided a HTTP Query Params, return the result in a given
// struct format.
func queryForLinks(
	ctx context.Context,
	cancel context.CancelFunc,
	httpClient *clients.HTTPClient,
	qp map[string]string,
	links interface{}) error {

	defer cancel()
	bodyInBytes, err := httpClient.Get(ctx, qp)
	if err != nil {
		return err
	}
	// Parse the JSON query result
	if err = json.Unmarshal(bodyInBytes, &links); err != nil {
		return err
	}
	return nil
}

// appendLinksToSlice : Takes an array of Link struct and convert it to an
// an array of strings.
func appendLinksToSlice(links []Link, titles *[]string) {
	// Using a hashmap to remove any possible duplicates
	uniqueLinks := make(map[string]int)
	for _, link := range links {
		if link.Title != "" {
			uniqueLinks[strings.TrimSpace(link.Title)] = 1
		}
	}
	for key := range uniqueLinks {
		*titles = append(*titles, key)
	}
}
