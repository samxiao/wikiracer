package wiki

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"sort"

	"golang.org/x/net/context"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	log "github.com/sirupsen/logrus"
)

const (
	queryConfigFilePath = "./config_test.yaml"
)

type QueryTestSuite struct {
	suite.Suite
	Client          *Query
	Server          *httptest.Server
	LastRequest     *http.Request
	LastRequestBody string
	ResponseFunc    func(http.ResponseWriter, *http.Request)
}

func (s *QueryTestSuite) SetupSuite() {
	logger := log.New()

	// Initialize a Query client
	w, err := NewQuery(logger, queryConfigFilePath)
	assert.NotNil(s.T(), w)
	assert.Nil(s.T(), err)
	s.Client = w

	// HTTPTest server
	s.Server = httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			body, _ := ioutil.ReadAll(r.Body)
			s.LastRequestBody = string(body)
			s.LastRequest = r
			if s.ResponseFunc != nil {
				s.ResponseFunc(w, r)
			}
		}))

	s.Client.httpClient.APIEndPoint = s.Server.URL
}

func (s *QueryTestSuite) TearDownSuite() {
	s.Server.Close()
}

func (s *QueryTestSuite) SetupTest() {
	s.ResponseFunc = nil
	s.LastRequest = nil
}

func TestMySuite(t *testing.T) {
	suite.Run(t, new(QueryTestSuite))
}

func (s *QueryTestSuite) TestForwardFetchTitles() {

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	var forwardResult01 = `{
    "continue": {
        "continue": "||",
        "plcontinue": "44209778|0|Alibaba_Group"
    },
    "query": {
        "pageids": [
            "44209778"
        ],
        "pages": {
            "44209778": {
                "links": [
                    {
                        "ns": 0,
                        "title": "7 Cups"
                    },
                    {
                        "ns": 0,
                        "title": "9GAG"
                    },
                    {
                        "ns": 0,
                        "title": "Airbnb"
                    },
                    {
                        "ns": 0,
                        "title": "Alexa Internet"
                    },
                    {
                        "ns": 0,
                        "title": "Algolia"
                    }
                ],
                "ns": 0,
                "pageid": 44209778,
                "title": "GitLab"
            }
        }
    }
}`

	var forwardResult02 = `{
    "batchcomplete": "",
    "limits": {
        "links": 500
    },
    "query": {
        "pageids": [
            "44209778"
        ],
        "pages": {
            "44209778": {
                "links": [
                    {
                        "ns": 0,
                        "title": "World"
                    },
                    {
                        "ns": 0,
                        "title": "Wufoo"
                    },
                    {
                        "ns": 0,
                        "title": "Xobni"
                    },
                    {
                        "ns": 0,
                        "title": "Y Combinator (company)"
                    },
                    {
                        "ns": 0,
                        "title": "Zapier"
                    },
                    {
                        "ns": 0,
                        "title": "Zenefits"
                    },
                    {
                        "ns": 0,
                        "title": "ZeroCater"
                    }
                ],
                "ns": 0,
                "pageid": 44209778,
                "title": "GitLab"
            }
        }
    }
}`
	// setup
	s.ResponseFunc = func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			if r.URL.String() == "/?action=query&format=json&indexpageids=true&pldir=ascending&pllimit=max&plnamespace=0&prop=links&titles=GitLab" {
				w.Write([]byte(forwardResult01))
			} else if r.URL.String() == "/?action=query&format=json&indexpageids=true&plcontinue=44209778%7C0%7CAlibaba_Group&pldir=ascending&pllimit=max&plnamespace=0&prop=links&titles=GitLab" {
				w.Write([]byte(forwardResult02))
			}
			w.WriteHeader(http.StatusOK)
		}
	}

	// test
	expectedTitles := []string{"7 Cups", "9GAG", "Airbnb", "Alexa Internet", "Algolia", "World", "Wufoo", "Xobni", "Y Combinator (company)", "Zapier", "Zenefits", "ZeroCater"}

	title := "GitLab"
	var titles []string
	err := s.Client.ForwardFetchTitles(
		ctx,
		cancel,
		map[string]string{},
		title,
		&titles)
	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), titles)

	assert.Equal(s.T(), len(titles), 12)

	// sort and compare with expected
	sort.Slice(expectedTitles, func(i, j int) bool { return expectedTitles[i] < expectedTitles[j] })
	sort.Slice(titles, func(i, j int) bool { return titles[i] < titles[j] })
	assert.Equal(s.T(), titles, expectedTitles)
}

func (s *QueryTestSuite) TestBackwardFetchTitles() {

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	var backwardResult01 = `{
    "continue": {
        "continue": "||",
        "lhcontinue": "27155990"
    },
    "query": {
        "pageids": [
            "45495529"
        ],
        "pages": {
            "45495529": {
                "linkshere": [
                    {
                        "ns": 0,
                        "pageid": 33109,
                        "title": "Wearable computer"
                    },
                    {
                        "ns": 0,
                        "pageid": 1110028,
                        "title": "Ruputer"
                    },
                    {
                        "ns": 0,
                        "pageid": 2867523,
                        "title": "Calculator watch"
                    },
                    {
                        "ns": 0,
                        "pageid": 8827953,
                        "title": "Casio Databank"
                    },
                    {
                        "ns": 0,
                        "pageid": 9015451,
                        "title": "Timex Datalink"
                    }
                ],
                "ns": 0,
                "pageid": 45495529,
                "title": "Pebble Time"
            }
        }
    }
}`

	var backwardResult02 = `{
    "batchcomplete": "",
    "limits": {
        "linkshere": 500
    },
    "query": {
        "pageids": [
            "45495529"
        ],
        "pages": {
            "45495529": {
                "linkshere": [
                    {
                        "ns": 0,
                        "pageid": 53462669,
                        "title": "Huawei Watch 2"
                    },
                    {
                        "ns": 0,
                        "pageid": 53672328,
                        "title": "Samsung Gear Fit 2"
                    }
                ],
                "ns": 0,
                "pageid": 45495529,
                "title": "Pebble Time"
            }
        }
    }
}`

	// setup
	s.ResponseFunc = func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			if r.URL.String() == "/?action=query&format=json&indexpageids=true&lhlimit=max&lhnamespace=0&prop=linkshere&titles=Pebble+Time" {
				w.Write([]byte(backwardResult01))
			} else if r.URL.String() == "/?action=query&format=json&indexpageids=true&lhcontinue=27155990&lhlimit=max&lhnamespace=0&prop=linkshere&titles=Pebble+Time" {
				w.Write([]byte(backwardResult02))
			}
			w.WriteHeader(http.StatusOK)
		}
	}

	// test
	expectedTitles := []string{"Wearable computer", "Ruputer", "Calculator watch", "Casio Databank", "Timex Datalink", "Huawei Watch 2", "Samsung Gear Fit 2"}

	title := "Pebble Time"
	var titles []string
	err := s.Client.BackwardFetchTitles(
		ctx,
		cancel,
		map[string]string{},
		title,
		&titles)
	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), titles)

	assert.Equal(s.T(), len(titles), 7)

	// // sort and compare with expected
	sort.Slice(expectedTitles, func(i, j int) bool { return expectedTitles[i] < expectedTitles[j] })
	sort.Slice(titles, func(i, j int) bool { return titles[i] < titles[j] })
	assert.Equal(s.T(), titles, expectedTitles)
}
