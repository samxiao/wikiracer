package utils

import "sync"

// ConcurrentMap : A Goroutine-safe Map data structure
type ConcurrentMap struct {
	s map[string]string
	sync.RWMutex
}

// NewConcurrentMap : Constructor for a new concurrent map
func NewConcurrentMap() ConcurrentMap {
	return ConcurrentMap{
		map[string]string{},
		sync.RWMutex{}}
}

// Get : Same operation as map[value], but goroutine-safe
func (m *ConcurrentMap) Get(key string) (value string) {
	m.RLock()
	defer m.RUnlock()
	value, _ = m.s[key]
	return
}

// Set : Same operation as map[key]=value, but goroutine-safe
func (m *ConcurrentMap) Set(key, value string) {
	m.Lock()
	defer m.Unlock()
	m.s[key] = value
}

// Merge : Merging another map into the existing concurrent map
func (m *ConcurrentMap) Merge(anotherMap map[string]string) {
	m.Lock()
	defer m.Unlock()
	for k, v := range anotherMap {
		m.s[k] = v
	}
}

// List : Returns a map of the existing concurrent map
func (m *ConcurrentMap) List() map[string]string {
	m.Lock()
	defer m.Unlock()
	list := make(map[string]string)
	for k, v := range m.s {
		list[k] = v
	}
	return list
}
