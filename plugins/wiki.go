package plugins

import (
	"errors"

	log "github.com/sirupsen/logrus"

	"gitlab.com/samxiao/wikiracer/lib"
	"gitlab.com/samxiao/wikiracer/lib/wiki"

	"golang.org/x/net/context"
)

// WikiSearch : The entry point of Wiki plugin.  It takes a configuration file,
// a search query that contains start title page and end title page, constructs
// a path that leads start title page reach to end title page as a wiki race.
func WikiSearch(logger *log.Logger, configFilePath string, searchQuery *lib.SearchQuery) (lib.SearchResultGraph, error) {
	if searchQuery.StartTitle == "" {
		return nil, errors.New("Missing search term for starting title")
	}
	if searchQuery.EndTitle == "" {
		return nil, errors.New("Missing search term for ending title")
	}

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	// Setup a Wiki lib with all the configurations
	w, err := wiki.NewWiki(logger, configFilePath)
	if err != nil {
		return nil, err
	}
	if w == nil {
		return nil, err
	}
	// Final clean up
	defer w.ForwardCacheRedisClient.FlushDB(ctx)
	defer w.BackwardCacheRedisClient.FlushDB(ctx)
	defer w.StackRedisClient.FlushDB(ctx)

	q, err := wiki.NewQuery(logger, configFilePath)
	if err != nil {
		return nil, err
	}
	if q == nil {
		return nil, err
	}

	// A simple channel to be used by both forward and backward, which
	// goroutine finds it first to terminate the goroutine
	commonChan := make(chan string)

	// - - - Search Forward - - -
	logger.Debugln("Start : " + searchQuery.StartTitle)
	forwardDirection := &wiki.SearchDirection{
		Name:               "forward",
		CurrentRedisClient: w.ForwardCacheRedisClient,
		ReverseRedisClient: w.BackwardCacheRedisClient,
	}
	go func(chan string, *log.Logger) {
		result := w.Search(ctx, cancel, q, forwardDirection, searchQuery.StartTitle)
		if result != "" {
			logger.Debugln("Call ForwardSearch : ", result)
			commonChan <- result
		}
	}(commonChan, logger)

	// - - - Search Backward - - -
	logger.Debugln("End : " + searchQuery.EndTitle)
	backwardDirection := &wiki.SearchDirection{
		Name:               "backward",
		CurrentRedisClient: w.BackwardCacheRedisClient,
		ReverseRedisClient: w.ForwardCacheRedisClient,
	}
	go func(chan string, *log.Logger) {
		result := w.Search(ctx, cancel, q, backwardDirection, searchQuery.EndTitle)
		if result != "" {
			logger.Debugln("Call BackwardSearch : ", result)
			commonChan <- result
		}
	}(commonChan, logger)

	// Await for both forward and backward to be finished
	common := <-commonChan
	logger.Debugln("Common mid point: ", common)

	// Construct the graph
	graph, err := w.BuildSearchResultGraph(
		ctx,
		searchQuery.StartTitle,
		searchQuery.EndTitle)
	if err != nil {
		return nil, err
	}

	return graph, nil
}
