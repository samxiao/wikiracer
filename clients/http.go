package clients

import (
	"io/ioutil"
	"net/http"
	"time"

	"golang.org/x/net/context"
)

type (
	// HTTPClient : a wrapper object for some common wikiracer operations
	HTTPClient struct {
		APIEndPoint string
		UserAgent   string
		Timeout     int
	}
)

// Get : Making RESTful GET calls with query params
func (h *HTTPClient) Get(
	ctx context.Context,
	queryParams map[string]string) ([]byte, error) {

	var err error
	var request *http.Request
	var response *http.Response
	var body []byte

	var httpClient = &http.Client{Timeout: time.Second * time.Duration(h.Timeout)}

	request, err = http.NewRequest(http.MethodGet, h.APIEndPoint, nil)
	request.Header.Set("User-Agent", h.UserAgent)
	if err != nil {
		return nil, err
	}

	// build the query
	query := request.URL.Query()
	for key, value := range queryParams {
		query.Add(key, value)
	}
	request.URL.RawQuery = query.Encode()

	// TODO: Will investigate this about
	// cancelling within goroutine.
	// request = request.WithContext(ctx)
	response, err = httpClient.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	// check response body for err
	body, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}
