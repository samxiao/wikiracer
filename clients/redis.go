package clients

import (
	"github.com/go-redis/redis"

	"golang.org/x/net/context"
)

const (
	redisKeyExpirationTime = 0 // Zero means the key has no expiration time.
)

// RedisClient : A wrapper of Go-Redis client that we can simplify several
// common Wikirace operations. This is holding some commonly used functions.
// It is also used for easier unit testing.
type RedisClient interface {
	Len(string) int
	Push(string, string) error
	Pop(string) (string, error)
	Insert(string, string) error
	Get(string) (string, error)
	Set(string, string) error
	Contains(string) (bool, error)
	FlushDB() error
	Close() error
}

// Redis : A wrapper that holds a redis client instance.
type Redis struct {
	client *redis.Client
}

// NewRedisClient : A new RedisClient instance
func NewRedisClient(
	address string,
	password string,
	db int) (*Redis, error) {

	c := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       db,
	})
	_, err := c.Ping().Result()
	if err != nil {
		return nil, err
	}
	r := &Redis{
		client: c,
	}
	return r, nil
}

// FlushDB : Clear up all the data on a specific Redis DB
func (r *Redis) FlushDB(ctx context.Context) error {
	r.client.WithContext(ctx)
	return r.client.FlushDB().Err()
}

// Get : Redis as a Hashmap with a Get operation.
func (r *Redis) Get(ctx context.Context, key string) (string, error) {
	r.client.WithContext(ctx)
	return r.client.Get(key).Result()
}

// Set : Redis as a Hashmap with a Set operation.
func (r *Redis) Set(ctx context.Context, key string, value string) error {
	r.client.WithContext(ctx)
	return r.client.Set(key, value, redisKeyExpirationTime).Err()
}

// Len : Redis' LLEN()
func (r *Redis) Len(ctx context.Context, key string) int {
	r.client.WithContext(ctx)
	i, err := r.client.LLen(key).Result()
	if err == redis.Nil {
		return 0
	} else if err != nil {
		return 0
	} else {
		return int(i)
	}
}

// Push : Redis' LPush()
func (r *Redis) Push(ctx context.Context, key string, value string) error {
	r.client.WithContext(ctx)
	return r.client.LPush(key, value).Err()
}

// Pop : Redis' LPop()
func (r *Redis) Pop(ctx context.Context, key string) (string, error) {
	r.client.WithContext(ctx)
	value, err := r.client.LPop(key).Result()
	if err == redis.Nil {
		return "", nil
	} else if err != nil {
		return "", err
	} else {
		return value, nil
	}
}

// Contains :  A wrapper on Get(), but simply cares whether a Key has existed
func (r *Redis) Contains(ctx context.Context, key string) bool {
	r.client.WithContext(ctx)
	_, err := r.client.Get(key).Result()
	if err == redis.Nil {
		return false
	} else if err != nil {
		return false
	}
	return true
}
