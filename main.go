package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"

	"gitlab.com/samxiao/wikiracer/lib"
	"gitlab.com/samxiao/wikiracer/plugins"

	log "github.com/sirupsen/logrus"
)

var logLevel string
var startTitle string
var endTitle string
var versionFlag bool
var configFilePath string

// Version : the version of this CLI executable was built
var Version string

// BuildTime : the timestamp that this CLI executable was built
var BuildTime string

// CommitSHA1 : commit Git SHA1 that this CLI executable was built
var CommitSHA1 string

type baseConfig struct {
	Plugin string `yaml:"plugin"`
}

func init() {
	flag.StringVar(&logLevel, "loglevel", "info", "Log level")
	flag.StringVar(&startTitle, "starttitle", "", "Start Wiki page to start the search")
	flag.StringVar(&endTitle, "endtitle", "", "Target Wiki page to reach to.")
	flag.StringVar(&configFilePath, "config", "./config.yaml", "File path for the configuration file in YAML format.")
	flag.BoolVar(&versionFlag, "version", false, "Current version of this tool")
}
func usage() {
	fmt.Fprintf(os.Stderr, "usage: %s -starttitle \"FOO\" -endtitle \"BAR\" -config \"./config.yaml\"\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	flag.Parse()

	if versionFlag {
		fmt.Fprintf(os.Stdout, "Version: %v\n", Version)
		fmt.Fprintf(os.Stdout, "Build: %v\n", BuildTime)
		fmt.Fprintf(os.Stdout, "Commit: %v\n", CommitSHA1)
		os.Exit(0)
	}

	if startTitle == "" {
		usage()
		os.Exit(1)
	}
	if endTitle == "" {
		usage()
		os.Exit(1)
	}
	if len(os.Args) < 2 {
		usage()
		os.Exit(1)
	}

	searchQuery := &lib.SearchQuery{
		StartTitle: startTitle,
		EndTitle:   endTitle,
	}

	// Convert it to absolute file path
	absConfigFilePath, err := filepath.Abs(configFilePath)
	if err != nil {
		panic(err)
	}

	logger := log.New()
	logger.WithFields(log.Fields{
		"start":      searchQuery.StartTitle,
		"end":        searchQuery.EndTitle,
		"configPath": absConfigFilePath,
	})
	logger.Formatter = &log.JSONFormatter{}
	lvl, _ := log.ParseLevel(logLevel)
	logger.Level = lvl

	// Loading configs
	var config baseConfig
	data, err := ioutil.ReadFile(absConfigFilePath)
	if err != nil {
		logger.Panicln(err)
	}
	if err := yaml.Unmarshal(data, &config); err != nil {
		logger.Panicln(err)
	}

	// Use config to determine which plugin to be used
	switch config.Plugin {
	case "wiki":
		graph, err := plugins.WikiSearch(logger, absConfigFilePath, searchQuery)
		if err != nil {
			logger.Panicln(err)
		}
		if graph != nil && len(graph) > 0 {
			logger.Println("Graph:")
			for i := range graph {
				logger.Printf("%v ➔ %v", graph[i].Src, graph[i].Dest)
			}
		} else {
			logger.Errorln("Not found!")
			os.Exit(0)
		}
	default:
		logger.Panicln("unrecognized plugin type")
	}

}
