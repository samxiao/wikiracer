.PHONY: all

SOURCES=$(shell find . \( -path ./vendor -o -path ./.git \) -prune -o -type d -print)
ALL_DIRS=$(shell find . \( -path ./vendor -o -path ./.git \) -prune -o -type d -print)
GO_PKGS=$(foreach pkg, $(shell go list ./...), $(if $(findstring /vendor/, $(pkg)), , $(pkg)))

EXECUTABLE=wikirace

VERSION=1.0.0
BUILD_TIME=`date +%FT%T%z`
COMMIT=`git rev-parse --short HEAD`

LDFLAGS=-ldflags "-w -s -X main.Version=${VERSION} -X main.BuildTime=${BUILD_TIME} -X main.CommitSHA1=${COMMIT}"

.DEFAULT_GOAL := all

fmt:
	@for pkg in $(GO_PKGS); do \
		cmd="go fmt $$pkg"; \
		eval $$cmd; \
		if test $$? -ne 0; then \
				exit 1; \
		fi; \
  done

lint:
	@golint ./... | grep -v '^vendor\/' || true

test:
	@for pkg in $(GO_PKGS); do \
		cmd="go test -race -v $$pkg"; \
		eval $$cmd; \
		if test $$? -ne 0; then \
				exit 1; \
		fi; \
  done

build: $(EXECUTABLE)

all: fmt lint test $(EXECUTABLE)

$(EXECUTABLE): $(SOURCES)
	@go build ${LDFLAGS} -o ${EXECUTABLE} main.go

.PHONY: clean
clean:
	if [ -f ${EXECUTABLE} ] ; then rm ${EXECUTABLE} ; fi
